library(tidyverse)

source("./source/scripts/_import.r")

# bootstrap
boot <- function(dx, index="subjectId", R=1000, stat=function(x) mean(x$rsp)) {
  dx %>%
    pull(index) ->
    ix
  ix %>%
    table() ->
    nx
  nx %>%
    length() ->
    nid

  out <- data.frame()

  for (i in 1:R) {
    cat("i =", i, "\n")
    sample(       x = ix
          ,    size = nid
          , replace = TRUE
          ) ->
      set
    bind_rows(lapply(1:nid, function(j) data.frame(dx[dx[, index] == set[j], ]))) ->
      ds
    rbind(out, stat(ds)) ->
      out
  }
  out
}

# compute group (delay/probability) -specific aggregate measures
summarize_by_group <- function(.x, group, uncert=0, var="x1") {
  .x %>%
    group_by(!!sym(group)) %>%
    summarize( !!sym(var) := mean(mean)
             ,        ymin = quantile(mean, .025)
             ,        ymax = quantile(mean, .975)
             , UNCERTAINTY = uncert
             )
}

# aggregator time
stat_time <- function(.x) {
  .x %>%
    group_by(t2) %>%
    summarize( mean=mean(x1) )
}

# aggregator risk
stat_risk <- function(.x) {
  .x %>%
    group_by(ph2) %>%
    summarize( mean=mean(rsp) )
}

# -------------------------------------------------------

cat("======== TIME ========\n")

# reference: the present
time[1,] %>%
  mutate(          x2 = 1
        ,          x1 = 1
        ,          t1 = 0
        ,          t2 = 0
        , UNCERTAINTY = FALSE
        ) ->
  refT

# process time data
time %>%
  filter(t1==0) %>%
  mutate(x1=x1/x2) %>%
  bind_rows(., refT, refT %>%
                      mutate(UNCERTAINTY=TRUE)
           ) %>%
  mutate( UNCERTAINTY = UNCERTAINTY %>%
                          as.numeric() %>%
                          as.factor()
        ,          t2 = t2 %>%
                          as.factor()
        ) ->
  timeY

# map over uncertainty groups
map_dfr(0:1, function(i) {
  cat(">> UNCERTAINTY =", i, "<<\n")
  boot(timeY %>% filter(UNCERTAINTY==i), stat=stat_time) %>%
    summarize_by_group(group="t2", uncert=i)
}) %>%
  write_csv("./intermediate/boot-time-1000.csv")


# -------------------------------------------------------


cat("======== RISK ========\n")

# references: impossibility and certainty
risk[1:2,] %>%
  mutate(         xh2 = c(1, 1)
        ,         xl2 = c(0, 0)
        ,         ph2 = c(0, 1)
        ,          x1 = c(0, 1)
        , uncertainty = FALSE
        ) ->
  refR

# process risk data
risk %>%
  bind_rows(., refR, refR %>%
            mutate(UNCERTAINTY=TRUE)
           ) %>%
  mutate( rsp = (x1-xl2)/(xh2-xl2)
        ) %>%
  mutate( UNCERTAINTY = UNCERTAINTY %>%
                          as.numeric() %>%
                          as.factor()
        ) ->
  riskY

# map over uncertainty groups
map_dfr(0:1, function(i) {
  cat(">> UNCERTAINTY =", i, "<<\n")
  boot(riskY %>% filter(UNCERTAINTY==i), stat=stat_risk) %>%
    summarize_by_group(group="ph2", uncert=i, var="rsp")
}) %>%
  write_csv("./intermediate/boot-risk-1000.csv")
