## Extract the parameters
# calculate individual effects on original scale (plural)
extractPars <- function(par=rhoi, values=draws) {
  posterior <- calculate(par, values=values)
  summary(posterior)[["statistics"]][, "Mean"]
}

## generate a 1-indexed continuous identifier
rx <- function(v) {
  w <- unique(v)
  match(v, w)
}

## test monotonicity
mfun <- function(x, y) {
  nax <- x %>% is.numeric()
  nay <- y %>% is.numeric()

  ifelse(nax, x, 1e6) > ifelse(nay, y, 0)
}

## utility function (power)
u <- function(x, rho) { x^rho }
## inverse utility function (inverse power)
ui <- function(y, rho) { y^(1/rho) }
## probability weighting function (Prelec-II)
w <- function(p, alpha, beta) { exp(-beta*(-log(p))^alpha) }
## time discount function (exponential)
d <- function(t, eta) { exp(-eta*t) }

## expit function
expit <- function(x) { 1/(1+exp(-x)) }
