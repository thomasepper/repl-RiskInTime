# import data; avoiding silencing imports for compatibility with legacy shell
"./data/tar2-time.csv" %>%
  read_csv() ->
  time
"./data/tar2-risk.csv" %>%
  read_csv() ->
  risk
"./data/tar2-survey.csv" %>%
  read_csv() %>%
  mutate(  participant = percParticipant > 0
        ,      mailing = percMailing > 0
        , experimenter = percExperimenter > 0
        , otherFactors = percOtherFactors > 0
        ,  UNCERTAINTY = participant | mailing | experimenter | otherFactors
  ) ->
  survey

# merge those subject with both data
inner_join(time, survey, by="subjectId") ->
  time
inner_join(risk, survey, by="subjectId") ->
  risk
