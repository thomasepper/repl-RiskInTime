## libraries
library(tidyverse)
library(greta)

## overrides
`%<>%` <- magrittr::`%<>%`
rename <- dplyr::rename

## imports
source("./source/scripts/_funs.r")
source("./source/scripts/_import.r")

## fix the RNG
set.seed(567)

## restricting attention to subjects who pass a simple test of first-order stochastic dominance
risk %>%
  filter(xh2==60 & xl2==20) %>%
  group_by(subjectId) %>%
  summarize(mono = mfun(x1[ph2==.95], x1[ph2==.05])) %>%
  filter(mono) %>%
  pull(subjectId) ->
  pass
### -apply to risk
risk %<>%
  filter(subjectId %in% pass) %>%
  arrange(subjectId)
### apply to time
time %<>%
  filter(subjectId %in% pass) %>%
  arrange(subjectId)

## estimate
source("./source/scripts/doHB-sequential.r")

## export
est %>%
  write_csv(path="./intermediate/pars-sequential.csv")
