# counts
ns <- length(unique(risk$subjectId))

# extractions
idr <- risk$subjectId %>% rx()
xr1 <- risk$x1
ph2 <- risk$ph2
xh2 <- risk$xh2
xl2 <- risk$xl2

idt <- time$subjectId %>% rx()
xt1 <- time$x1
t1  <- time$t1/12
t2  <- time$t2/12
x2  <- time$x2


## STAGE 1: RISK

# parameters risk
rhom    <- normal(log(1), 5)
alpham  <- normal(log(1), 5)
betam   <- normal(log(1), 5)
kapparm <- normal(log(1), 1)

rhos    <- lognormal(1, 1)
alphas  <- lognormal(1, 1)
betas   <- lognormal(1, 1)
kappars <- lognormal(1, 1)

rhoiP    <- normal(0, 1, dim = ns)
alphaiP  <- normal(0, 1, dim = ns)
betaiP   <- normal(0, 1, dim = ns)
kappariP <- normal(0, 1, dim = ns)
rhoi     <- exp(rhom + rhos*rhoiP)
alphai   <- exp(alpham + alphas*alphaiP)
betai    <- exp(betam + betas*betaiP)
kappari  <- exp(kapparm + kappars*kappariP)

# model risk
distribution(xr1) <- normal( ui( w(ph2, alphai[idr], betai[idr])*u(xh2, rhoi[idr]) + (1 - w(ph2, alphai[idr], betai[idr]))*u(xl2, rhoi[idr]) , rhoi[idr] ), (xh2-xl2)*kappari[idr] )

mR <- model(rhom, alpham, betam, kapparm, rhos, alphas, betas, kappars, rhoiP, alphaiP, betaiP, kappariP)

# draws risk
drawsR <- mcmc(mR, n_samples = 10000, chains = 10, warmup = 10000)

# posteriors risk
posterior_rhoi    <- extractPars(rhoi, drawsR)
posterior_alphai  <- extractPars(alphai, drawsR)
posterior_betai   <- extractPars(betai, drawsR)
posterior_kappari <- extractPars(kappari, drawsR)

# results risk
tibble( subjectId = unique(risk$subjectId)
      ,        id = 1:ns
      ,      rhoi = posterior_rhoi
      ,    alphai = posterior_alphai
      ,     betai = posterior_betai
      ,   kappari = posterior_kappari
      ) %>%
  mutate(
    UNCERTAINTY = subjectId %in% (risk %>% filter(UNCERTAINTY) %>% pull(subjectId))
  ) ->
  estr


## STAGE 2: TIME

# indices
it0 <- t1==0
it1 <- t1>1/12

# parameters time
etam    <- normal(log(1), 5)
sm      <- normal(0, 1.5)
kappatm <- normal(log(1), 1)

etas    <- lognormal(1, 1)
ss      <- lognormal(1, 1.5)
kappats <- lognormal(1, 1)

etaiP    <- normal(0, 1, dim = ns)
siP      <- normal(0, 1, dim = ns)
kappatiP <- normal(0, 1, dim = ns)

etai    <- exp(etam + etas*etaiP)
si      <- expit(sm + ss*siP)
kappati <- exp(kappatm + kappats*kappatiP)

# model time; plugging in parameters estimated in risk stage
distribution(xt1[it0]) <- normal( ui( d(t2[it0]-t1[it0], etai[idt[it0]]) * w(si[idt[it0]]^t2[it0], estr[idt[it0], ]$alphai, estr[idt[it0], ]$betai) * u(x2[it0], estr[idt[it0], ]$rhoi), estr[idt[it0], ]$rhoi), x2[it0]*kappati[idt[it0]] )
distribution(xt1[it1]) <- normal( ui( d(t2[it1]-t1[it1], etai[idt[it1]]) * w(si[idt[it1]]^t2[it1], estr[idt[it1], ]$alphai, estr[idt[it1], ]$betai)/w(si[idt[it1]]^t1[it1], estr[idt[it1], ]$alphai, estr[idt[it1], ]$betai) * u(x2[it1], estr[idt[it1], ]$rhoi), estr[idt[it1], ]$rhoi), x2[it1]*kappati[idt[it1]] )

mT <- model(etam, sm, kappatm, etas, ss, kappats, etaiP, siP, kappatiP)

# draws time
drawsT <- mcmc(mT, n_samples = 10000, chains = 10, warmup = 10000)

# posteriors time
posterior_etai    <- extractPars(etai, drawsT)
posterior_si      <- extractPars(si, drawsT)
posterior_kappati <- extractPars(kappati, drawsT)

# results all (risk and time)
tibble( subjectId = unique(time$subjectId)
      ,        id = 1:ns
      ,      rhoi = posterior_rhoi
      ,    alphai = posterior_alphai
      ,     betai = posterior_betai
      ,      etai = posterior_etai
      ,        si = posterior_si
      ,   kappati = posterior_kappati
      ,   kappari = posterior_kappari
      ) %>%
  mutate( UNCERTAINTY = subjectId %in% (time %>%
                                          filter(UNCERTAINTY) %>%
                                          pull(subjectId) %>%
                                          unique()
                                       ) %>% as.numeric()
        ) ->
  est
