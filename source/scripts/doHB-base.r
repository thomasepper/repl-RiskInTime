# counts
ns <- length(unique(time$subjectId))

# extractions
id <- time$subjectId %>% rx()
x1 <- time$x1
t1 <- time$t1/12
t2 <- time$t2/12
x2 <- time$x2

# assumptions (fixed)
falpha <- .5
fbeta  <- .95
frho   <- .8
feta   <- .1

# parameters
sm     <- normal(0, 1.5)
kappam <- normal(log(1), 1)

ss     <- lognormal(1, 1.5)
kappas <- lognormal(1, 1)

siP     <- normal(0, 1, dim = ns)
kappaiP <- normal(0, 1, dim = ns)

si     <- expit(sm + ss*siP)
kappai <- exp(kappam + kappas*kappaiP)

# indices
it0 <- t1==0
it1 <- t1>1/12

# model
distribution(x1[it0]) <- normal( ui( d(t2[it0]-t1[it0], feta) * w(si[id[it0]]^t2[it0], falpha, fbeta) * u(x2[it0], frho), frho), x2[it0]*kappai[id[it0]] )
distribution(x1[it1]) <- normal( ui( d(t2[it1]-t1[it1], feta) * w(si[id[it1]]^t2[it1], falpha, fbeta)/w(si[id[it1]]^t1[it1], falpha, fbeta) * u(x2[it1], frho), frho), x2[it1]*kappai[id[it1]] )

m <- model(sm, kappam, ss, kappas, siP, kappaiP)

# draws
draws <- mcmc(m, n_samples = 10000, chains = 10, warmup = 10000)

# posteriors
posterior_si     <- extractPars(si, draws)
posterior_kappai <- extractPars(kappai, draws)

# results
tibble( subjectId = unique(time$subjectId)
      ,        id = 1:ns
      ,        si = posterior_si
      ,    kappai = posterior_kappai
      ) %>%
  mutate( UNCERTAINTY = subjectId %in% (time %>%
                                          filter(UNCERTAINTY) %>%
                                          pull(subjectId)
                                       ) %>% as.numeric()
        ) ->
  est
