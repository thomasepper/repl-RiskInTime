## libraries
library(tidyverse)
library(greta)

## overrides
`%<>%` <- magrittr::`%<>%`
rename <- dplyr::rename

## imports
source("./source/scripts/_funs.r")
source("./source/scripts/_import.r")

## fix the RNG
set.seed(567)

## estimate
source("./source/scripts/doHB-base.r")

## export
est %>%
  write_csv(path="./intermediate/pars-base.csv")
