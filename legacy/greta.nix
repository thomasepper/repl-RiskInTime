{ pkgs ? import (fetchTarball "https://github.com/NixOS/nixpkgs/archive/5272327b81ed355bbed5659b8d303cf2979b6953.tar.gz") {} }:

with pkgs;
let
  my-callr = pkgs.rPackages.callr.overrideAttrs (oldAttrs: rec {
    version = "3.4.1";
    src = pkgs.fetchurl {
        url = "https://cran.r-project.org/src/contrib/Archive/callr/callr_${version}.tar.gz";
        sha256 = "sha256-4HizPc2pDYIScV17XREG3uYBcWr/5NrF/4S0bydQy1s=";
    };
  });

  r-with-packages = rWrapper.override {
    packages = with rPackages; let
    in [
      tidyverse
      greta
      xtable
    ];
  };
in
mkShell {
  buildInputs = [
    python37Packages.tensorflow
    python37Packages.tensorflow-tensorboard
    python37Packages.tensorflow-probability
    r-with-packages
    my-callr
  ];
}
