#+TITLE:               Risk in Time: The Intertwined Nature of Risk Taking and Time Discounting
#+SUBTITLE:            README
#+AUTHOR:              Thomas F. Epper $\quad$ Helga Fehr-Duda
#+DATE:
#+LATEX_CLASS:         article
#+LATEX_CLASS_OPTIONS: [a4paper, 10pt]
#+LATEX_HEADER:        \usepackage[utf8]{inputenc}
#+LATEX_HEADER:        \usepackage[T1]{fontenc}
#+LATEX_HEADER:        \usepackage{graphicx}
#+LATEX_HEADER:        \usepackage{grffile}
#+LATEX_HEADER:        \usepackage{longtable}
#+LATEX_HEADER:        \usepackage{wrapfig}
#+LATEX_HEADER:        \usepackage{rotating}
#+LATEX_HEADER:        \usepackage[normalem]{ulem}
#+LATEX_HEADER:        \usepackage{amsmath}
#+LATEX_HEADER:        \usepackage{textcomp}
#+LATEX_HEADER:        \usepackage{amssymb}
#+LATEX_HEADER:        \usepackage{capt-of}
#+LATEX_HEADER:        \usepackage{hyperref}
#+LATEX_HEADER:        \usepackage{tikz}
#+LATEX_HEADER:        \usepackage{fullpage}
#+LATEX_HEADER:        \usepackage{tabularx}
#+LATEX_HEADER:        \usepackage{float}
#+LATEX_HEADER:        \usepackage{mathtools}
#+LATEX_HEADER:        \usepackage[sc]{mathpazo}
#+LATEX_HEADER:        \usepackage{unicode-math}
#+LATEX_HEADER:        \usepackage{geometry}
#+LATEX_HEADER:        \usepackage{xfrac}
#+LATEX_HEADER:        \usepackage{tcolorbox}
#+OPTIONS:             toc:nil
#+PROPERTY:            header-args:R :session *RSession* :results output :exports results :dir test :tangle yes


* Directory structure

This reproduction package contains the following files and folders:

#+begin_src
README.org      the source of this readme
README.pdf      a pdf export of this readme

CITATION        details on how to cite this work/paper
flake.nix       the flake declaring the reproducible build environment and
                commands (see the details below)
flake.lock      the lock pinning/snapshotting all inputs (dependencies), i.e.
                software and package versions, used in this project

source/         all source files
source/scripts/ additional R scripts for bootstrapping and model estimation
intermediate/   the intermediate outputs (CSVs and TeX-files) created when
                running the reproduction steps
results/        the generated pdf-reports with the results

data/           the folder containing all data used in the calibration and
                evaluation exercises (Section 4)
legacy/         legacy shell configuration that makes available the packages
                required for estimating the models (Section 4.9)
#+end_src

* Reproduction pipeline

** Prerequisites

There are two dependencies that need to be installed on the machine running the reproduction steps:
1. [[https://nixos.wiki/wiki/Nix_package_manager][the Nix package manager]] A detailed description on how to install it and use it can be found below.
2. [[https://git-scm.com/][git]]: Some dependencies are retrieved via git and the package has to be part of a git repository.

There are two scenarios:

*** Scenario 1: Starting with the zip archive of the reproduction package available via the publisher's website

This package's content needs to be part of a [[https://git-scm.com/][git]] repository before the reproduction steps can be initiated.
To do this, initialize, add and commit the content of the package.
Ensure that the repository's root corresponds to the location of this README document.
For instance, on a Unix-like system (e.g., Linux, BSD, macOS), navigate to the reproduction package's root directory, type and run

#+begin_src bash
git init . && git add -A && git commit -m "Initial commit"
#+end_src

in a shell.

*** Scenario 2: Cloning the Gitlab repository

Clone the repository by typing

#+begin_src bash
git clone https://gitlab.com/thomasepper/repl-RiskInTime/
#+end_src

into the command line.

Once these requirements are met, the rest of the dependencies (including the exact versions of the packages) will be pulled and built via the mechanism described below.


** Flakes

We use `Nix Flakes` for reproducibility (see https://nixos.wiki/wiki/Flakes for more information).
Flakes provide a fully reproducible environment pinning the exact versions of each package to produce coherent results.
The environment runs on various operating systems which are compatible with the `Nix package manager`.
Instructions on how to install `Nix: the package manager` can be found here: https://nixos.org/download.html.
All code supplied within this package has been built and tested on x86-64 GNU/Linux systems.

Once the `nix` command is available, activate `flakes`.
There are two ways to do this:

1. *(recommended)* Activate flake support globally by pasting the following command in the shell:

   #+begin_src bash
   mkdir -p ~/.config/nix
   echo "experimental-features = nix-command flakes" >> ~/.config/nix/nix.conf
   #+end_src
2. Decorate all future `nix` calls (i.e. `nix develop` and `nix run ...`) with the following argument:

   #+begin_src bash
   --experimental-features 'nix-command flakes'
   #+end_src

   For example:

   #+begin_src bash
   nix develop --experimental-features 'nix-command flakes'
   #+end_src

Then open a terminal emulator, change to the directory where this README document is located, and start a development shell by typing `nix develop`.
To see how to run the available reproduction steps, see the next section.

/(To automate the start of the development shell, install [[https://direnv.net/][direnv]] and activate it via typing `direnv allow` in the root of the project package. The `.envrc` file stored in the root ensures that a development shell is automatically invoked on entering the directory. The `nix develop` step therefore becomes obsolete and the applications become directly available from within the shell.)/

Note that it takes some time to start the development shell for the first time as all relevant dependencies have to be pulled (and - depending on the system - possibly also be partly compiled).
Another important thing to know is that the reproduction pipeline ensures that the packages are available only locally.
Hence, anything installed within this project will not affect other versions installed on the system.

The development shell will make available [[https://www.r-project.org][R]], [[https://www.python.org][Python]], [[https://www.gnu.org/software/emacs][GNU Emacs]] and [[https://www.latex-project.org/-files][pdfLaTeX]].
The Emacs LISP engine is used to compile the org notebooks in headless mode.
The compilation step executes the R code chunks within these files and tangles the results into intermediate files.
The intermediate files are regular [[https://www.latex-project.org/][tex]]-files.
The final results are generated by compiling the tex files into pdf-files using [[https://www.latex-project.org/-files][pdfLaTeX]].
The estimation of the hierarchical Bayesian models uses an R frontend ([[https://greta-stats.org/][greta]]) interfacing with a Python backend ([[https://www.tensorflow.org/probability][TensorFlow Probability]]).

All packages used in this project are under open source licenses.

The reproducibility pipeline takes an _input_ (stored in [[./source][./source]]) to generate one or more (temporary) artefact(s) (stored in [[./intermediate][./intermediate]]) from which the _output_ (stored in [[./results][./results]]) is produced.
The source files are [[https://orgmode.org][org]]-files with inline [[https://www.r-project.org][R]] and [[https://www.latex-project.org/][LaTeX]] code.
The R source code depends on various packages, including [[https://www.tidyverse.org][tidyverse]] and [[https://cran.r-project.org/web/packages/xtable/][xtable]].
Various illustrations are produced using [[https://tikz.dev/][TikZ]].

* Reproduction instructions

The [[./flake.nix][flake.nix]] available in the project root defines all relevant apps to produce the targets and a set of auxiliary commands.
All reproduction steps can be invoked via the command line from the project's root directory (i.e. the location of this README file) _after the development shell has been started_ (via `nix develop`; but see the instructions above).

The following apps and commands are available from within the command line (see the respective entries in the flake declaration) once a development shell has been started (see instructions above):

 - *illustrate:* Produces all the figures and trees in the main (theoretical) part of the paper.
   - To run, use `nix run .#illustrate`
 - *calibrate:* Runs all calibrations presented in Section 4.9.
   - To run, use `nix run .#calibrate`
 - *bootstrap:* Produce the 95% confidence bands depicted in Figure 10. Stores the results as csv-files in the `./intermediate/` folder.
   - To run, use `nix run .#bootstrap`
 - *estimate:* Executes the estimation pipeline to obtain the individual parameters. Stores the results as csv-files in the `./intermediate` folder.
   - To run, use `nix run .#estimate`
 - *evaluate-only:* Compiles the report with existing bootstrap and estimation results in place. Requires that `boostrap` and `estimate` ran prior to it.
   - To run, use `nix run .#evaluate-only`
 - *evaluate:* Runs the pipeline `estimate` -> `bootstrap` -> `evaluate-only` sequentially to produce all inputs and compile the complete report.
   - To run, use `nix run .#evaluate`

Note that all code producing the outputs can be run manually without using the provisioned pipeline.
To do this, ensure that the exact versions of the packages are available on the machine.
These versions can be obtained by investigating the sources tagged in the [[./flake.lock]] and [[./legacy/greta.nix]].

The following auxiliary shell scripts are available within the development shell:

- *flush:* Removes all intermediate and results files.
- *tidy-up:* Moves the TeX-files and pdfs into `./intermediate/` or `./results/`, respectively. Mainly for internal use (see `flakes.nix`).

Execution of the auxiliary scripts does _not_ require a preceding `nix run ...`!
