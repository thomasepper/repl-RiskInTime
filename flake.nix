{
  description = "Reproducible build environment for Epper and Fehr-Duda (2023) 'Risk in Time: The Intertwined Nature of Risk Taking and Time Discounting'";

  inputs.nixpkgs.url = "github:NixOS/nixpkgs/release-23.05";

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = import nixpkgs { inherit system; };

      illustrate = pkgs.writeShellScriptBin "illustrate" ''
        echo "Compiling illustrations..."
        build-org ./source/illustration.org
        tidy-up
      '';

      calibrate = pkgs.writeShellScriptBin "calibrate" ''
        echo "Calibrating..."
        build-org ./source/calibration.org
        tidy-up
      '';

      estimate = pkgs.writeShellScriptBin "estimate" ''
        echo "Invoking a legacy shell..."
        echo "Estimating base model..."
        nix-shell ./legacy/greta.nix --run "Rscript ./source/scripts/run-base.r"
        echo "Estimating extended model..."
        nix-shell ./legacy/greta.nix --run "Rscript ./source/scripts/run-sequential.r"
      '';

      bootstrap = pkgs.writeShellScriptBin "bootstrap" ''
        echo "Bootstrapping..."
        Rscript ./source/scripts/boot.r
      '';

      evaluate-only = pkgs.writeShellScriptBin "evaluate-only" ''
        echo "Compiling report..."
        build-org ./source/evaluation.org
        tidy-up
      '';

      evaluate = pkgs.writeShellScriptBin "evaluate" ''
        ${estimate}/bin/estimate
        ${bootstrap}/bin/bootstrap
        ${evaluate-only}/bin/evaluate-only
      '';

      r-with-packages = pkgs.rWrapper.override {
        packages = with pkgs.rPackages; [
          tidyverse
          gridExtra
          xtable
          tikzDevice
          scales
        ];
      };

      emacs-with-packages = pkgs.emacsPackagesNg.emacsWithPackages (epkgs: [
        epkgs.org
        epkgs.ess
      ]);

      emacs-config = pkgs.writeText "env-init.el" ''
        (require 'package)
        (add-to-list 'package-archives
                     '("melpa" . "https://melpa.org/packages/") t)
        (package-initialize)
        (unless package-archive-contents
          (package-refresh-contents))
        (unless (package-installed-p 'use-package)
          (package-install 'use-package))
        (require 'use-package)
        (use-package org
          :ensure t)
        (use-package ess
          :ensure t)
        (setq org-confirm-babel-evaluate nil)
        (org-babel-do-load-languages
         'org-babel-load-languages
         '((R . t)
           (shell . t)
           (latex . t)))
        (defun my-export-to-pdf ()
          (org-babel-execute-buffer)
          (org-latex-export-to-pdf))
      '';

      build-org-script = pkgs.writeShellScriptBin "build-org" ''
        file="$1"
        base="''${file%%.*}"
        emacs --batch -l ${emacs-config} "$file" --eval "(my-export-to-pdf)"
        echo "Exported to $base.pdf"
      '';

      tidy-script = pkgs.writeShellScriptBin "tidy-up" ''
        mv ./source/*.tex ./intermediate/
        mv ./source/*.pdf ./results/
        rm ./*.tex
      '';

      flush-script = pkgs.writeShellScriptBin "flush" ''
        rm ./intermediate/*
        rm ./results/*
      '';

    in {
      defaultPackage = pkgs.mkShell {
        buildInputs = [
          pkgs.texlive.combined.scheme-full
          r-with-packages
          emacs-with-packages
          build-org-script
          tidy-script
          flush-script
        ];
        shellHook = ''
          alias emacs='${emacs-with-packages}/bin/emacs -q -l $EMACS_CONFIG'
        '';
      };

      apps = {
        illustrate    = flake-utils.lib.mkApp { drv = illustrate; };
        calibrate     = flake-utils.lib.mkApp { drv = calibrate; };
        estimate      = flake-utils.lib.mkApp { drv = estimate; };
        bootstrap     = flake-utils.lib.mkApp { drv = bootstrap; };
        evaluate-only = flake-utils.lib.mkApp { drv = evaluate-only; };
        evaluate      = flake-utils.lib.mkApp { drv = evaluate; };
      };
    });
}
